package com.example.movies.ui.detail

import android.widget.Toast
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max
import androidx.compose.ui.unit.min
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import coil.request.CachePolicy
import com.example.domain.film.Film
import com.example.movies.R
import com.example.movies.util.DateTimeUtils.convertToLocalDate
import com.google.accompanist.insets.systemBarsPadding
import kotlinx.coroutines.launch
import kotlin.math.floor
import kotlin.math.max

const val POSTER_HEIGHT = 150
const val BACKDROP_HEIGHT = 300

@Composable
fun FilmDetailPage(viewModel: FilmDetailViewModel, id: String) {
    val viewState by viewModel.baseLiveData.collectAsState()
    val errorState by viewModel.errorLiveData.collectAsState()
    LaunchedEffect(id) {
        viewModel.loadFilm(id, this)
    }

    errorState?.message?.let {
        Toast.makeText(LocalContext.current, it, Toast.LENGTH_LONG).show()
    }

    viewState?.data?.let { film ->
        val scrollState = rememberScrollState()
        var offset = 0f
        val screenHeight =
            (LocalConfiguration.current.screenHeightDp * LocalConfiguration.current.densityDpi / 160f)

        offset += scrollState.value.toFloat() / screenHeight

        Surface(Modifier.fillMaxSize()) {
            Column(modifier = Modifier.systemBarsPadding(top = true, bottom = false)) {
                DetailAppBar(film.title.orEmpty())
                Box {
                    BackDrop(offset, film)
                    Column(modifier = Modifier.verticalScroll(scrollState)) {
                        Spacer(modifier = Modifier.height((BACKDROP_HEIGHT - (POSTER_HEIGHT / 2)).dp))
                        Poster(viewModel, offset, film)
                        Spacer(modifier = Modifier.height(2.dp))
                        Column(
                            modifier = Modifier
                                .padding(start = 20.dp, end = 20.dp)
                        ) {
                            Text(
                                text = "${film.releasedOn?.convertToLocalDate()?.year} | ${film.length} | ${film.director}",
                                color = Color.Gray,
                                style = MaterialTheme.typography.subtitle1,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = 20.dp)
                            )

                            Text(
                                text = "Cast: ${film.cast}",
                                style = MaterialTheme.typography.subtitle1,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = 20.dp)
                            )
                            Text(
                                text = "${film.overview}",
                                style = MaterialTheme.typography.body1,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = 20.dp)
                            )
                            Spacer(modifier = Modifier.height(BACKDROP_HEIGHT.dp * 2))
                        }
                    }
                }
            }
        }

    }
}

@Composable
private fun BackDrop(scrollOffset: Float, film: Film) {
    val imageSize by animateDpAsState(
        targetValue = BACKDROP_HEIGHT.dp - max(
            0.dp,
            BACKDROP_HEIGHT.dp * scrollOffset * 4
        )
    )
    Image(
        painter = rememberImagePainter(
            data = film.backdrop,
            builder = {
                crossfade(true)
                memoryCachePolicy(policy = CachePolicy.ENABLED)
            }
        ),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .fillMaxWidth()
            .height(imageSize)
            .graphicsLayer {
                // translationY = 100 * scrollOffset
            }
    )
}

@Composable
private fun Poster(viewModel: FilmDetailViewModel, scrollOffset: Float, film: Film) {
    val imageSize by animateDpAsState(
        targetValue = POSTER_HEIGHT.dp - min(
            (POSTER_HEIGHT / 2).dp,
            POSTER_HEIGHT.dp * scrollOffset
        )
    )
    val dynamicLines = max(1, (2f - scrollOffset * 10).toInt())
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(Color(0x80000000))
    ) {
        Image(
            painter = rememberImagePainter(
                data = film.poster,
                builder = {
                    crossfade(true)
                    memoryCachePolicy(policy = CachePolicy.ENABLED)
                }
            ),
            modifier = Modifier
                .size(imageSize),
            contentDescription = "Poster"
        )
        Column(
            modifier = Modifier
                .height(IntrinsicSize.Min)
                .fillMaxWidth()
                .align(alignment = Alignment.Top),

            verticalArrangement = Arrangement.Top,
        ) {
            BookmarkView(viewModel, film, Modifier.align(Alignment.End))
            Text(
                text = "${film.title} (${film.imdbRating})",
                style = MaterialTheme.typography.h5,
                fontSize = 20.sp,
                maxLines = dynamicLines
            )
            RatingView(film.imdbRating ?: 0f)
        }
    }
}

@Composable
private fun RatingView(rating: Float) {
    val numberYellowStar = floor(rating / 2).toInt()
    Row(modifier = Modifier.padding(top = 10.dp)) {
        for (i in 0 until numberYellowStar) {
            StarView(true)
        }
        for (i in 0 until (5 - numberYellowStar)) {
            StarView(false)
        }
    }
}

@Composable
private fun StarView(isYellow: Boolean) {
    Image(
        painter = painterResource(if (isYellow) R.drawable.ic_star_yellow else R.drawable.ic_star_gray),
        contentDescription = null,
        modifier = Modifier
            .height(20.dp)
            .width(20.dp)
            .padding(end = 5.dp)
    )
}

@Composable
fun DetailAppBar(
    filmTitle: String,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = {
            Row {
                Text(
                    text = filmTitle,
                    modifier = Modifier
                        .padding(start = 4.dp)
                        .heightIn(max = 24.dp)
                )
            }
        },
        backgroundColor = MaterialTheme.colors.surface.copy(alpha = 0.87f),
        modifier = modifier
    )
}

@Composable
fun BookmarkView(viewModel: FilmDetailViewModel, film: Film, modifier: Modifier) {
    val coroutineScope = rememberCoroutineScope()
    Image(
        painter = painterResource(if (film.isBookmarked) R.drawable.ic_heart_red else R.drawable.ic_heart_gray),
        contentDescription = null,
        modifier = modifier
            .height(50.dp)
            .width(50.dp)
            .padding(10.dp)
            .clickable {
                coroutineScope.launch {
                    viewModel.updateBookmark(film.id, !film.isBookmarked, this)
                }
            }
    )
}
