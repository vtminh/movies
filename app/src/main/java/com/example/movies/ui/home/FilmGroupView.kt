package com.example.movies.ui.home

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

@Composable
fun FilmGroupView(
    viewModel: FilmGroupViewModel,
    textState: MutableState<TextFieldValue>,
    modifier: Modifier = Modifier,
    navigateToDetail: (String) -> Unit
) {
    val errorState by viewModel.errorLiveData.collectAsState()
    errorState?.let {
        Toast.makeText(LocalContext.current, it.message, Toast.LENGTH_LONG).show()
    }
    val loadingState by viewModel.loadingLiveData.collectAsState()
    if (loadingState) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            CircularProgressIndicator()
        }
        return
    }

    FilmGroup(modifier, viewModel, textState, navigateToDetail)
}

@Composable
fun FilmGroup(
    modifier: Modifier,
    viewModel: FilmGroupViewModel,
    textState: MutableState<TextFieldValue>,
    navigateToDetail: (String) -> Unit
) {
    val isRefreshing by viewModel.isRefreshing.collectAsState()

    SwipeRefresh(
        state = rememberSwipeRefreshState(isRefreshing),
        onRefresh = { viewModel.refresh() },
    ) {
        Column(modifier) {
            val films by (if (textState.value.text.isEmpty())
                viewModel.baseLiveData else viewModel.searchResultFilmGroup)
                .collectAsState()
            films?.data.takeIf { !it.isNullOrEmpty() }?.let { filmGroups ->
                Spacer(Modifier.height(8.dp))
                LazyColumn {
                    items(items = filmGroups, itemContent = { filmGroup ->
                        Genre(
                            name = filmGroup.titleGenre,
                            films = filmGroup.filmList,
                            navigateToDetail = navigateToDetail,
                            modifier = Modifier
                                .fillMaxSize()
                                .weight(1f)
                        )
                    })
                }
            } ?: kotlin.run {
                Text(
                    "No data", textAlign = TextAlign.Center, modifier =
                    Modifier
                        .width(IntrinsicSize.Max)
                        .height(IntrinsicSize.Max)
                )
            }
        }
    }

}