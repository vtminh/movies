package com.example.movies.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import com.example.movies.R
import com.google.accompanist.insets.statusBarsHeight
import com.google.accompanist.insets.systemBarsPadding
import com.google.accompanist.pager.ExperimentalPagerApi
import kotlinx.coroutines.launch

@Composable
fun Home(
    viewModel: FilmGroupViewModel,
    navigateToDetail: (String) -> Unit
) {
    Surface(Modifier.fillMaxSize()) {
        HomeContent(
            viewModel,
            navigateToDetail = navigateToDetail,
            modifier = Modifier.fillMaxSize()
        )
    }
}

@Composable
fun HomeAppBar(
    viewModel: FilmGroupViewModel,
    state: MutableState<TextFieldValue>,
    backgroundColor: Color,
    modifier: Modifier = Modifier
) {
    val coroutineScope = rememberCoroutineScope()
    val focusRequester = remember { FocusRequester() }
    TopAppBar(
        title = {
            Row {
                Image(
                    painter = painterResource(R.drawable.ic_logo),
                    contentDescription = null,
                    modifier = Modifier.align(Alignment.CenterVertically)
                )
                Text(
                    text = stringResource(R.string.app_name),
                    modifier = Modifier
                        .padding(start = 4.dp)
                        .heightIn(max = 24.dp)
                        .align(Alignment.CenterVertically)
                )

                TextField(
                    value = state.value,
                    onValueChange = { value ->
                        state.value = value
                        coroutineScope.launch {
                            viewModel.searchInDb(value.text, this)
                        }
                    },
                    modifier = Modifier
                        .align(Alignment.Bottom)
                        .fillMaxWidth()
                        .focusRequester(focusRequester),
                    textStyle = MaterialTheme.typography.body1,
                    trailingIcon = {
                        IconButton(
                            onClick = {
                                focusRequester.requestFocus()
                            }
                        ) {
                            Icon(
                                Icons.Default.Search,
                                contentDescription = "",
                                modifier = Modifier
                                    .padding(15.dp)
                                    .size(24.dp)
                            )
                        }
                    },
                    singleLine = true,
                    shape = RectangleShape,
                    colors = TextFieldDefaults.textFieldColors(
                        textColor = Color.White,
                        cursorColor = Color.White,
                        leadingIconColor = Color.White,
                        trailingIconColor = Color.White,
                        backgroundColor = MaterialTheme.colors.surface,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent
                    )
                )
            }
        },
        backgroundColor = backgroundColor,
        modifier = modifier
    )
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun HomeContent(
    viewModel: FilmGroupViewModel,
    modifier: Modifier = Modifier,
    navigateToDetail: (String) -> Unit
) {
    val textState = remember { mutableStateOf(TextFieldValue("")) }

    Column(
        modifier = modifier
            .systemBarsPadding(top = false, bottom = false)
            .fillMaxWidth()
    ) {
        val appBarColor = MaterialTheme.colors.surface.copy(alpha = 0.87f)
        Spacer(
            Modifier
                .background(appBarColor)
                .fillMaxWidth()
                .statusBarsHeight()
        )
        HomeAppBar(
            viewModel,
            textState,
            backgroundColor = appBarColor,
            modifier = Modifier.fillMaxWidth()
        )
        FilmGroupView(
            viewModel,
            textState,
            Modifier.fillMaxWidth(),
            navigateToDetail = navigateToDetail
        )
    }
}