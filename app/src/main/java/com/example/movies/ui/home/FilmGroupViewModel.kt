package com.example.movies.ui.home

import com.example.domain.base.DataResponse
import com.example.domain.film.Film
import com.example.domain.film.FilmGroup
import com.example.domain.film.GetFilmListUseCase
import com.example.domain.film.SearchFilmsInLocalUseCase
import com.example.movies.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class FilmGroupViewModel(
    private val getFilmListUseCase: GetFilmListUseCase,
    private val searchFilmsInLocalUseCase: SearchFilmsInLocalUseCase
) : BaseViewModel<List<FilmGroup>>() {

    val isRefreshing = MutableStateFlow<Boolean>(false)
    val searchResultFilmGroup = MutableStateFlow<DataResponse<List<FilmGroup>>?>(null)

    init {
        loadData(getFilmListUseCase)
    }

    fun refresh() {
        loadData(getFilmListUseCase)
    }

    fun searchInDb(text: String, scope: CoroutineScope){
        searchFilmsInLocalUseCase.text = text
        loadData(searchFilmsInLocalUseCase, searchResultFilmGroup, scope = scope)
    }
}