
package com.example.movies.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable

@Composable
fun MoviesTheme(
    content: @Composable () -> Unit
) {
    MaterialTheme(
        colors = MoviesColors,
        typography = MoviesTypography,
        shapes = MoviesShapes,
        content = content
    )
}
