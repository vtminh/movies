package com.example.movies.ui.detail

import com.example.domain.film.Film
import com.example.domain.film.GetFilmDetailUseCase
import com.example.domain.film.UpdateBookmarkedUseCase
import com.example.movies.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class FilmDetailViewModel(
    private val getFilmDetailUseCase: GetFilmDetailUseCase,
    private val updateBookmarkedUseCase: UpdateBookmarkedUseCase
) : BaseViewModel<Film>() {

    fun loadFilm(id: String, scope: CoroutineScope) {
        getFilmDetailUseCase.id = id
        loadData(getFilmDetailUseCase, scope = scope)
    }

    fun updateBookmark(id: String, isBookmarked: Boolean, scope: CoroutineScope) {
        updateBookmarkedUseCase.apply {
            this.id = id
            this.isBookmarked = isBookmarked
        }
        scope.launch {
            updateBookmarkedUseCase.execute().collect{}
        }

    }
}