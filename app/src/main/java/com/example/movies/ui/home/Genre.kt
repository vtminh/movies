package com.example.movies.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.domain.film.Film
import com.example.movies.ui.theme.Keyline1
import androidx.compose.foundation.lazy.items
import androidx.compose.ui.Alignment
import androidx.compose.ui.draw.paint
import androidx.compose.ui.res.painterResource
import coil.request.CachePolicy
import com.example.movies.R
import kotlinx.coroutines.launch

@Composable
fun Genre(
    name: String,
    films: List<Film>?,
    navigateToDetail: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Text(
            modifier = Modifier.padding(start = Keyline1),
            text = name,
            style = MaterialTheme.typography.body2
        )
        FilmList(
            films = films,
            onItemClicked = navigateToDetail,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
private fun FilmList(
    films: List<Film>?,
    onItemClicked: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyRow(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(10.dp),
        contentPadding = PaddingValues(start = Keyline1, top = 8.dp, end = Keyline1, bottom = 24.dp)
    ) {
        items(items = films.orEmpty()) { film ->
            FilmItem(
                film = film,
                modifier = Modifier.width(128.dp),
                onItemClicked = onItemClicked
            )
        }
    }
}

@Composable
private fun FilmItem(
    modifier: Modifier = Modifier,
    film: Film,
    onItemClicked: (String) -> Unit
) {
    Box(modifier = modifier.fillMaxSize()) {
        Image(
            painter = rememberImagePainter(
                data = film.poster,
                builder = {
                    crossfade(true)
                    memoryCachePolicy(policy = CachePolicy.ENABLED)
                }
            ),
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxSize()
                .aspectRatio(0.8f)
                .clip(MaterialTheme.shapes.medium)
                .clickable {
                    onItemClicked(film.id)
                },
        )

        Image(
            painter = painterResource(if (film.isBookmarked) R.drawable.ic_heart_red else R.drawable.ic_heart_gray),
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.TopEnd)
                .height(40.dp)
                .width(40.dp)
                .padding(10.dp)

        )
    }

}