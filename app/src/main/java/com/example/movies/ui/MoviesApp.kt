package com.example.movies.ui

import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.movies.MyApplication
import com.example.movies.R
import com.example.movies.ui.detail.FilmDetailPage
import com.example.movies.ui.detail.FilmDetailViewModel
import com.example.movies.ui.home.Home
import com.example.movies.ui.home.FilmGroupViewModel
import com.example.movies.util.DevicePosture
import com.example.movies.util.ViewModelUtils
import kotlinx.coroutines.flow.StateFlow

@Composable
fun MoviesApp(
    devicePosture: StateFlow<DevicePosture>,
    appState: MoviesAppState = rememberMoviesAppState()
) {
    (LocalContext.current as? MainActivity).run {
        if (appState.isOnline) {
            NavHost(
                navController = appState.navController,
                startDestination = Screen.Home.route
            ) {
                composable(Screen.Home.route) { backStackEntry ->

                    val component = (this@run?.application as MyApplication).getAppComponent()
                    val viewModel: FilmGroupViewModel = ViewModelUtils.daggerViewModel {
                        component.getFilmGroupViewModel()
                    }
                    Home(
                        viewModel,
                        navigateToDetail = { id ->
                            appState.navigateToDetail(id, backStackEntry)
                        }
                    )
                }

                composable(
                    route = Screen.Detail.route,
                    arguments = listOf(navArgument("id") {
                        type = NavType.StringType; defaultValue = "0"
                    }),
                ) {
                    val id = it.arguments?.getString("id")

                    val component = (this@run?.application as MyApplication).getAppComponent()
                    val viewModel: FilmDetailViewModel = ViewModelUtils.daggerViewModel {
                        component.getFilmDetailViewModel()
                    }

                    FilmDetailPage(viewModel, id.orEmpty())

                }

            }
        } else {
            OfflineDialog { appState.refreshOnline() }
        }
    }

}

@Composable
fun OfflineDialog(onRetry: () -> Unit) {
    AlertDialog(
        onDismissRequest = {},
        title = { Text(text = stringResource(R.string.connection_error_title)) },
        text = { Text(text = stringResource(R.string.connection_error_message)) },
        confirmButton = {
            TextButton(onClick = onRetry) {
                Text(stringResource(R.string.retry_label))
            }
        }
    )
}
