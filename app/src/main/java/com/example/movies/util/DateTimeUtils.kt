package com.example.movies.util

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

object DateTimeUtils {
    fun String.convertToLocalDate(format: String = "yyyy-MM-dd'T'HH:mm:ss"): LocalDate {
        val formatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern(format, Locale.ENGLISH)
        return LocalDate.parse(this, formatter)
    }
}