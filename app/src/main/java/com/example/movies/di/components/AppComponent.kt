package com.example.movies.di.components

import android.app.Application
import com.example.domain.di.ApplicationScope
import com.example.framework.module.DatabaseModule
import com.example.framework.module.NetworkModule
import com.example.framework.module.RepositoryModule
import com.example.movies.MyApplication
import com.example.movies.di.modules.ViewModelModule
import com.example.movies.ui.detail.FilmDetailViewModel
import com.example.movies.ui.home.FilmGroupViewModel
import dagger.BindsInstance
import dagger.Component

/**
 * @author tuanminh.vu
 */
@ApplicationScope
@Component(
    modules = [
        NetworkModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        DatabaseModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(application: MyApplication)
    fun getFilmGroupViewModel(): FilmGroupViewModel
    fun getFilmDetailViewModel(): FilmDetailViewModel

}