package com.example.movies.di.modules

import com.example.domain.di.ApplicationScope
import com.example.domain.film.GetFilmDetailUseCase
import com.example.domain.film.GetFilmListUseCase
import com.example.domain.film.SearchFilmsInLocalUseCase
import com.example.domain.film.UpdateBookmarkedUseCase
import com.example.movies.ui.detail.FilmDetailViewModel
import com.example.movies.ui.home.FilmGroupViewModel
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {
    @Provides
    @ApplicationScope
    fun provideFilmGroupViewModel(
        getFilmListUseCase: GetFilmListUseCase,
        searchFilmsInLocalUseCase: SearchFilmsInLocalUseCase
    ): FilmGroupViewModel =
        FilmGroupViewModel(getFilmListUseCase, searchFilmsInLocalUseCase)

    @Provides
    @ApplicationScope
    fun provideFilmDetailViewModel(
        getFilmDetailUseCase: GetFilmDetailUseCase,
        updateBookmarkedUseCase: UpdateBookmarkedUseCase
    ): FilmDetailViewModel =
        FilmDetailViewModel(getFilmDetailUseCase, updateBookmarkedUseCase)

}