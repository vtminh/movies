package com.example.movies.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.base.BaseUseCase
import com.example.domain.base.DataResponse
import com.example.domain.base.UseCaseDispatcher
import com.example.domain.base.UseCaseDispatcherImpl
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

/**
 * @author tuanminh.vu
 */
open class BaseViewModel<T>() : ViewModel() {

    val baseLiveData = MutableStateFlow<DataResponse<T>?>(null)
    val loadingLiveData = MutableStateFlow<Boolean>(false)
    val errorLiveData = MutableStateFlow<Throwable?>(null)

    /**
     * @param useCase load data from remote or local
     * @param liveData emitted result
     * @param loadingLiveData emitted loading state
     * @param errorLiveData emitted error if any
     * @param shouldUseCache tell the dataSource to load from local database or not
     */
    fun <S : DataResponse<T>> loadData(
        useCase: BaseUseCase<S>,
        liveData: MutableStateFlow<DataResponse<T>?>? = baseLiveData,
        loadingLiveData: MutableStateFlow<Boolean>? = this.loadingLiveData,
        errorLiveData: MutableStateFlow<Throwable?>? = this.errorLiveData,
        shouldUseCache: Boolean = false,
        scope: CoroutineScope = viewModelScope
    ) {
        useCase.shouldUseCache = shouldUseCache
        if (!shouldUseCache)
            loadingLiveData?.value = true
        scope.launch {
            useCase.execute()
                .catch { e ->
                    e.printStackTrace()
                    loadingLiveData?.value = false
                    errorLiveData?.value = e
                }
                .collect {
                    if (it.isFromCache && !it.isSuccess()) {
                        loadingLiveData?.value = true
                    } else if (it.isSuccess()) {
                        loadingLiveData?.value = false
                    }
                    if (!it.isFromCache && !it.isSuccess()) {
                        loadingLiveData?.value = false
                        errorLiveData?.value = it.errorMessage
                    }
                    liveData?.value = it
                }
        }
    }
}