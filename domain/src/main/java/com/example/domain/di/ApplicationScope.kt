package com.example.domain.di

import javax.inject.Scope

/**
 * @author tuanminh.vu
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope