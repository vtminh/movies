package com.example.domain.film

import com.example.domain.base.BaseUseCase
import com.example.domain.base.DataResponse
import com.example.domain.film.FilmGroup.Companion.convertToUIFilmGroupList
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SearchFilmsInLocalUseCase @Inject constructor(
    private val repository: FilmRepository
) : BaseUseCase<DataResponse<List<FilmGroup>>>() {

    var text: String = ""
    override fun run(): Flow<DataResponse<List<FilmGroup>>> {
        return repository.searchFilmInLocal(text).map {
            DataResponse(
                data = it.convertToUIFilmGroupList(),
                isFromCache = true
            )
        }
    }
}