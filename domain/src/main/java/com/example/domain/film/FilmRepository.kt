package com.example.domain.film

import kotlinx.coroutines.flow.Flow

interface FilmRepository {
    suspend fun getFilmListFromRemote(): FilmListResponse
    fun getFilmListFromLocal(): Flow<List<Film>>
    fun getFilmDetailFromLocal(id: String): Flow<Film>
    fun saveFilmListToDb(filmList: List<Film>): Flow<List<Long>>
    fun updateBookmarked(id: String, isBookmarked: Boolean): Flow<Int>
    fun searchFilmInLocal(text: String): Flow<List<Film>>
    fun getAllBookmarkFilmIdList(): Flow<List<String>>
}