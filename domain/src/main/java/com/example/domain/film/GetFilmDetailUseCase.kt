package com.example.domain.film

import com.example.domain.base.BaseUseCase
import com.example.domain.base.DataResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

class GetFilmDetailUseCase @Inject constructor(
    private val repository: FilmRepository
) : BaseUseCase<DataResponse<Film>>() {

    lateinit var id: String

    override fun run(): Flow<DataResponse<Film>> {
        return repository.getFilmDetailFromLocal(id)
            .map { DataResponse(data = it, isFromCache = true) }
    }
}