package com.example.domain.film

import com.example.domain.base.BaseDataModel
import com.google.gson.annotations.SerializedName

/**
 * @author tuanminh.vu
 */
data class FilmListResponse(
    @SerializedName("movies") val movies: List<Film>
) : BaseDataModel()

