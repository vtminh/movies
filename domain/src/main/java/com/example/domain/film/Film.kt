package com.example.domain.film

import com.example.domain.base.BaseDataModel
import com.google.gson.annotations.SerializedName

/**
 * @author tuanminh.vu
 */

data class Film(
    val id: String,
    val title: String? = null,
    val overview: String? = null,
    val genres: List<String>? = null,
    val cast: List<String>? = null,
    val poster: String? = null,
    val backdrop: String? = null,
    @SerializedName("released_on") val releasedOn: String? = null,
    val length: String? = null,
    @Transient var director: List<String>? = null,
    @SerializedName("imdb_rating") val imdbRating: Float? = 0f,
    var isBookmarked: Boolean = false
) : BaseDataModel()

