package com.example.domain.film

class FilmGroup(
    val titleGenre: String,
    val filmList: List<Film>
) {
    companion object {
        fun List<Film>?.convertToUIFilmGroupList(): List<FilmGroup> {
            val allGenres = HashSet<String>()
            this?.forEach {
                it.genres?.let {
                    allGenres.addAll(it)
                }
            }

            val genreMap = hashMapOf<String, MutableList<Film>>()
            allGenres.forEach {
                genreMap[it] = mutableListOf()
            }

            this?.forEach { film ->
                film.genres?.forEach { genre ->
                    genreMap[genre]?.add(film)
                }
            }

            return genreMap.toList().map {
                FilmGroup(it.first, it.second)
            }
        }
    }
}
