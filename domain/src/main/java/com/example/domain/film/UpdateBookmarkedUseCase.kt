package com.example.domain.film

import com.example.domain.base.BaseUseCase
import com.example.domain.base.DataResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

class UpdateBookmarkedUseCase @Inject constructor(
    private val repository: FilmRepository
) : BaseUseCase<DataResponse<Int>>() {

    lateinit var id: String
    var isBookmarked = false

    override fun run(): Flow<DataResponse<Int>> {
        return repository.updateBookmarked(id, isBookmarked)
            .map { DataResponse(data = it, isFromCache = true) }
    }
}