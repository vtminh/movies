package com.example.domain.film

import com.example.domain.base.BaseUseCase
import com.example.domain.base.DataResponse
import com.example.domain.film.FilmGroup.Companion.convertToUIFilmGroupList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class GetFilmListUseCase @Inject constructor(
    private val repository: FilmRepository
) : BaseUseCase<DataResponse<List<FilmGroup>>>() {

    override fun run(): Flow<DataResponse<List<FilmGroup>>> {
        return flow { emit(repository.getFilmListFromRemote()) }
            .flatMapConcat {
                repository.saveFilmListToDb(it.movies)
            }
            .flatMapConcat {
                repository.getFilmListFromLocal()
            }
            .map { DataResponse(it.convertToUIFilmGroupList()) }
    }
}