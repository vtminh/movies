package com.example.domain.base

import kotlinx.coroutines.Dispatchers

class UseCaseDispatcherImpl : UseCaseDispatcher {
    override fun getIOThread() = Dispatchers.IO
    override fun getMainThread() = Dispatchers.Main
    override fun getComputationThread() = Dispatchers.Default

}
