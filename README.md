# Movies app
#### _Ongoing movies in cinema_

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

## Features

- Get movie list from https://wookie.codesubmit.io
- Store movie list in database
- Support bookmark in local
- Support searching by film title

## Tech

Dillinger uses a number of open source projects to work properly:

- MVVM with Clean architecture, in which, Domain layer is pure Kotlin. 
- Room for database 
- Jetpack Compose for View layer
- Dagger 2 for DI & module structure
- Coroutine for Threading 
- Language: Kotlin

## Testing
All of modules, classes are designed to eligible for Unit Test

## Suggestion
- 'director' field in the API response is not consistent. It could be string or array. Clients have to parse it manually, but this could lead to time performance. It's better if BE side returns consistent data type. 

## Installation
Get apk file in: /app/build/intermediates/apk/debug/app-debug.apk

## License
Vu Tuan Minh
Email: vtminh1805@gmail.com
