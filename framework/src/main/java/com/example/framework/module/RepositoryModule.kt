package com.example.framework.module

import com.example.domain.film.FilmRepository
import com.example.framework.model.film.FilmRepositoryImpl
import com.example.framework.model.film.remote.FilmRemote
import com.example.domain.di.ApplicationScope
import com.example.framework.model.film.local.FilmLocal
import dagger.Module
import dagger.Provides

/**
 * @author tuanminh.vu
 */
@Module
open class RepositoryModule {

    @ApplicationScope
    @Provides
    fun provideFilmRepository(remote: FilmRemote, local: FilmLocal): FilmRepository {
        return FilmRepositoryImpl(remote, local)
    }

}