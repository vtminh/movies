package com.example.framework.module

import android.app.Application
import androidx.room.Room
import com.example.domain.di.ApplicationScope
import com.example.framework.db.FilmDatabase
import com.example.framework.model.film.local.FilmDao
import dagger.Module
import dagger.Provides

/**
 * @author tuanminh.vu
 */
@Module
open class DatabaseModule {

    @ApplicationScope
    @Provides
    fun provideFilmDao(filmDatabase: FilmDatabase): FilmDao {
        return filmDatabase.filmDao()
    }

    @ApplicationScope
    @Provides
    fun provideFilmDatabase(app: Application): FilmDatabase {
        return Room.databaseBuilder(app, FilmDatabase::class.java, "film.db")
            // This is not recommended for normal apps, but the goal of this sample isn't to
            // showcase all of Room.
            .fallbackToDestructiveMigration()
            .build()
    }

}