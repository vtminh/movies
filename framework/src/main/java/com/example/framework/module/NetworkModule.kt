package com.example.framework.module

import com.example.domain.base.Constants.WOOKIE_ENDPOINT
import com.example.domain.di.ApplicationScope
import com.example.domain.film.Film
import com.example.framework.db.MyDeserializer
import com.example.framework.model.FilmService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

/**
 * @author tuanminh.vu
 */

@Module
class NetworkModule {

    companion object {
        private const val WOOKIE_SERVICE_NAME = "WookieService"
        private const val REQUEST_TIME_OUT = 45L
    }

    @Provides
    @ApplicationScope
    @Named(WOOKIE_SERVICE_NAME)
    fun provideOKHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .readTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIME_OUT, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                request.header("Authorization", "Bearer Wookie2019")
                chain.proceed(request.build())
            }
            .addInterceptor { chain ->
                chain.proceed(chain.request())
            }

        return builder.build()
    }

    @Provides
    @ApplicationScope
    @Named(WOOKIE_SERVICE_NAME)
    fun provideRetrofit(
        @Named(WOOKIE_SERVICE_NAME) okHttpClient: OkHttpClient,
        gson: Gson
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(WOOKIE_ENDPOINT)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    @ApplicationScope
    fun provideUserService(@Named(WOOKIE_SERVICE_NAME) retrofit: Retrofit): FilmService {
        return retrofit.create(FilmService::class.java)
    }

    @Provides
    @ApplicationScope
    fun provideGson(): Gson {
        return GsonBuilder().registerTypeAdapter(Film::class.java, MyDeserializer()).setLenient()
            .create()
    }
}