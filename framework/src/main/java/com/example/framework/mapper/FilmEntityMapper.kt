package com.example.framework.mapper

import com.example.domain.film.Film
import com.example.framework.model.film.local.FilmEntity

object FilmEntityMapper {
    fun Film.toFilmEntity(): FilmEntity {
        return FilmEntity(
            id = this.id,
            title = this.title,
            overview = this.overview,
            genres = this.genres,
            cast = this.cast,
            poster = this.poster,
            backdrop = this.backdrop,
            year = this.releasedOn,
            length = this.length,
            director = this.director,
            imdbRating = this.imdbRating,
            isBookmarked = this.isBookmarked
        )
    }

    fun FilmEntity.toFilm(): Film {
        return Film(
            id = this.id,
            title = this.title,
            overview = this.overview,
            genres = this.genres,
            cast = this.cast,
            poster = this.poster,
            backdrop = this.backdrop,
            releasedOn = this.year,
            length = this.length,
            director = this.director,
            imdbRating = this.imdbRating,
            isBookmarked = this.isBookmarked
        )
    }
}