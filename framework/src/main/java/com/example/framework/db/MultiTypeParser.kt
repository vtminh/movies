package com.example.framework.db

import com.example.domain.film.Film
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class MyDeserializer : JsonDeserializer<Film> {
    @Throws(JsonParseException::class)
    override fun deserialize(arg0: JsonElement, arg1: Type, arg2: JsonDeserializationContext): Film {
        val decodeObj: JsonObject = arg0.asJsonObject
        val gson = Gson()
        val decode: Film = gson.fromJson(arg0, Film::class.java)
        val values: MutableList<String>
        if (decodeObj.get("director").isJsonArray) {
            values = gson.fromJson(
                decodeObj.get("director"),
                object : TypeToken<List<String>>() {}.type
            )
        } else {
            val single: String = gson.fromJson(decodeObj.get("director"), String::class.java)
            values = ArrayList()
            values.add(single)
        }
        decode.director = values
        return decode
    }
}