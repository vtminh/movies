package com.example.framework.db

import android.util.Log
import androidx.room.TypeConverter

class Converters {
    @TypeConverter
    fun fromStringList(value: List<String>?): String? {
        return value?.joinToString()
    }

    @TypeConverter
    fun toStringList(value: String): List<String> {
        return value.split(",")
    }
}