package com.example.framework.model.film.remote

import com.example.domain.film.FilmListResponse
import com.example.framework.model.FilmService
import com.example.framework.network.NetworkUtils
import javax.inject.Inject

/**
 * @author tuaninh.vu
 */

class FilmRemote @Inject constructor(private val service: FilmService) {

    fun getFilmList(): FilmListResponse {
        return NetworkUtils.handleServerResponse(service.getFilmList())
    }
}