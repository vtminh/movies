package com.example.framework.model.film.local

import com.example.domain.base.DataResponse
import com.example.domain.film.Film
import com.example.framework.mapper.FilmEntityMapper.toFilm
import com.example.framework.mapper.FilmEntityMapper.toFilmEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * @author tuaninh.vu
 */

class FilmLocal @Inject constructor(private val filmDao: FilmDao) {

    fun getFilmList(): Flow<List<Film>> {
        return filmDao.getFilmList().map {
            it.map { it.toFilm() }
        }
    }

    fun getFilm(id: String): Flow<Film> {
        return filmDao.getFilm(id).map { it.toFilm() }
    }

    fun saveFilmList(films: List<Film>): Flow<List<Long>> {
        return flow {
            val bookmarkedIdList = filmDao.getAllBookmarkedFilm()
            val insertNewList = filmDao.insertAll(films.map { it.toFilmEntity() })
            filmDao.restoreBookmark(bookmarkedIdList)
            emit(insertNewList)
        }
    }

    fun updateBookmarked(id: String, isBookmarked: Boolean): Flow<Int> {
        return flow { emit(filmDao.updateBookmark(id, isBookmarked)) }
    }

    fun searchFilmInLocal(text: String): Flow<List<Film>> {
        return filmDao.searchFilmInLocal(text).map { it.map { it.toFilm() } }
    }

    fun getAllBookmarkFilmIdList(): Flow<List<String>> {
        return flow { emit(filmDao.getAllBookmarkedFilm()) }
    }
}