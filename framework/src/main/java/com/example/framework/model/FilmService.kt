package com.example.framework.model

import com.example.domain.film.FilmListResponse
import retrofit2.Call
import retrofit2.http.GET

/**
 * @author tuanminh.vu
 */
interface FilmService {

    @GET("movies")
    fun getFilmList(): Call<FilmListResponse>
}