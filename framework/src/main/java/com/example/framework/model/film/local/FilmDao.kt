package com.example.framework.model.film.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.domain.film.Film
import kotlinx.coroutines.flow.Flow

@Dao
abstract class FilmDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertAll(films: List<FilmEntity>): List<Long>

    @Query("""SELECT * FROM film """)
    abstract fun getFilmList(): Flow<List<FilmEntity>>

    @Query("""SELECT * FROM film WHERE id = :id""")
    abstract fun getFilm(id: String): Flow<FilmEntity>

    @Query("""UPDATE film SET is_bookmarked = :isBookmarked WHERE id = :id""")
    abstract fun updateBookmark(id: String, isBookmarked: Boolean): Int

    @Query("""UPDATE film SET is_bookmarked = 1 WHERE id IN (:idList)""")
    abstract fun restoreBookmark(idList: List<String>): Int

    @Query("""SELECT film.id FROM film WHERE is_bookmarked = 1 """)
    abstract fun getAllBookmarkedFilm(): List<String>

    @Query("""SELECT * FROM film WHERE title LIKE '%' || :text || '%' """)
    abstract fun searchFilmInLocal(text: String): Flow<List<FilmEntity>>
}