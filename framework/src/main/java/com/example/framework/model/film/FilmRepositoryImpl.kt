package com.example.framework.model.film

import com.example.domain.base.DataResponse
import com.example.domain.film.Film
import com.example.domain.film.FilmListResponse
import com.example.domain.film.FilmRepository
import com.example.framework.model.film.local.FilmLocal
import com.example.framework.model.film.remote.FilmRemote
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map

/**
 * @author tuanminh.vu
 */
class FilmRepositoryImpl(
    private val remote: FilmRemote,
    private val local: FilmLocal
) : FilmRepository {

    override suspend fun getFilmListFromRemote(): FilmListResponse {
        return remote.getFilmList()
    }

    override fun getFilmListFromLocal(): Flow<List<Film>> {
        return local.getFilmList()
    }

    override fun getFilmDetailFromLocal(id: String): Flow<Film> {
        return local.getFilm(id)
    }

    override fun saveFilmListToDb(filmList: List<Film>): Flow<List<Long>> {
        return local.saveFilmList(filmList)
    }

    override fun updateBookmarked(id: String, isBookmarked: Boolean): Flow<Int> {
        return local.updateBookmarked(id, isBookmarked)
    }

    override fun searchFilmInLocal(text: String): Flow<List<Film>> {
        return local.searchFilmInLocal(text)
    }

    override fun getAllBookmarkFilmIdList(): Flow<List<String>> {
        return local.getAllBookmarkFilmIdList()
    }

}