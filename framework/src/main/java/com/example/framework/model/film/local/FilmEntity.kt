package com.example.framework.model.film.local

import androidx.room.*
import com.example.domain.base.BaseDataModel
import com.google.gson.annotations.SerializedName


@Entity(
    tableName = "film"
)
class FilmEntity(
    @PrimaryKey @ColumnInfo(name = "id") val id: String,
    @ColumnInfo(name = "title") val title: String? = null,
    @ColumnInfo(name = "overview") val overview: String? = null,
    @ColumnInfo(name = "genres") val genres: List<String>? = null,
    @ColumnInfo(name = "cast") val cast: List<String>? = null,
    @ColumnInfo(name = "poster") val poster: String? = null,
    @ColumnInfo(name = "backdrop") val backdrop: String? = null,
    @ColumnInfo(name = "year") val year: String? = null,
    @ColumnInfo(name = "length") val length: String? = null,
    @ColumnInfo(name = "director") val director: List<String>? = null,
    @ColumnInfo(name = "imdb_rating") val imdbRating: Float? = 0f,
    @ColumnInfo(name = "is_bookmarked") val isBookmarked: Boolean = false
) : BaseDataModel()